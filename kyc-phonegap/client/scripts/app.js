'use strict';

var app = angular.module('kyc-phonegap', ['ngRoute','timer','ngMessages','services','ui.bootstrap','ngTouch','ui.router']);


app.config(['$stateProvider', '$urlRouterProvider',function ($stateProvider,$urlRouterProvider) {
  console.log("App Config!");

// Stateprovider is probably replacing routerprovider in the future
    $urlRouterProvider.otherwise('/home');

    $stateProvider

      .state('home', {
        url: '/home',
        templateUrl: 'views/main.html', 
     })

      .state('team', {
        url: '/team',
        templateUrl: 'views/team.html',
      })

      .state('about', {
        url: '/about',
        templateUrl: 'views/about.html'
      })

      .state('scores', {
        url: '/scores',
        templateUrl: 'views/scores.html',
        controller: 'scoreController'
      })

      .state('quiz', {
        url: '/quiz',
        templateUrl: 'views/quiz.html',
        controller: 'gameController'
      })

      .state('endgame', {
        url: '/endgame',
        templateUrl: 'views/endgame.html',
        controller: 'scoreController'
      })

      .state('addQuestions', {
        url: '/addQuestions',
        templateUrl: 'views/addQuestions.html',
        controller: 'addQuestionController'
      })

      .state('ownQuiz', {
        url: '/ownQuiz',
        templateUrl: 'views/ownQuiz.html',
        controller: 'anotherGameController'
      })

      .state('parse', {
        url: '/parse',
        templateUrl: 'views/parser.html',
        controller: 'parseController'
      })

      .state('customGame', {
        url: '/customGame',
        templateUrl: 'views/customGame.html',
        controller: 'customGameController'
      });



/*

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        // controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        // controller: 'AboutCtrl'
      })
      .when('/scores', {
        templateUrl: 'views/scores.html',
        controller: 'scoreController'
      }) 
      .when('/quiz', {
        templateUrl: 'views/quiz.html',
        controller:  'gameController'
      }).when('/endgame', {
        templateUrl: 'views/endgame.html',
        controller: 'scoreController'
      }).when('/addQuestions', {
        templateUrl: 'views/addQuestions.html',
        controller: 'addQuestionController'
      }).when('/ownQuiz', {
        templateUrl: 'views/ownQuiz.html',
        controller: 'anotherGameController'
      }).when('/parse', {
        templateUrl: 'views/parser.html',
        controller: 'parseController'
      })
      .otherwise({
        redirectTo: '/'
    });

*/

}]);

app.run(function() {
/*
  var users = dbService2.getUsers().then(function(data) {
    console.log("Users length: " + data.length)
  });
*/

  console.log("App run " );

});

app.controller('appController',['$scope', '$location',function($scope, $location) {
  
  $scope.inMainMenu = function(path) {
    return $location.path() == path;
  }
}]);

app.directive('navbar', function() {
  return {
    restrict: 'E',
    templateUrl: 'views/navbar.html'
  }
});
