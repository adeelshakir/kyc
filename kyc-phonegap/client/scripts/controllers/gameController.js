'use strict';

angular.module('kyc-phonegap')
.controller('gameController',['$scope','$http','$location','scoreService','gameTypeService','$timeout','dbService','remoteService', 
    function($scope, $http,$location, scoreService, gameTypeService, $timeout,dbService, remoteService){
    console.log('Game Controller Initialized');


    $scope.answers = [];
    $scope.general = [];
    $scope.users = [];

    $scope.currentIndex = 0;
    $scope.currentUser = 0;
    $scope.lives = 3;
    $scope.points = 0; 
    $scope.questions = [];
    $scope.optionsShuffled = false;
    $scope.currentUser = 0;
    $scope.points = 0;
    $scope.currentOptions = [];
    $scope.isAnswered = 1;
    $scope.timerRunning = false;
    $scope.time = 0;

    var remote = remoteService.address();

    $scope.getAll = function(){
        $http.get(remote + '/general/all').success(function(data) {
            if(data){
                $scope.general = data.general;
                        $http.get(remote + '/answers/all').success(function(data) {
                            if(data){
                                $scope.answers = data.answers;
                            }
                        });         
            }
        });
    };

    dbService.users().then(function(data) {
        $scope.users = data.users;
    });

    $scope.getAll();

    // Set options for different game types. 
    $scope.gameSetup = function() {
        var game = gameTypeService.getType();
        
        if(game == 0) {
            $scope.lives = 3;
        }

        if(game == 1) {
            $scope.lives = -1;
        }

        if(game == 2) {
            $scope.lives = 1;
        }

        var currentIndexX = Math.floor(Math.random() * $scope.answers.length);
        $scope.currentIndex = currentIndexX;
    };

    $scope.changeToRandomIndex = function() {
        var currentIndexX = Math.floor(Math.random() * $scope.answers.length);
        $scope.currentIndex = currentIndexX;

    $scope.gameSetup();
    $scope.changeToRandomIndex();

    };



    /* functions for controlling the timer */
    $scope.startTimer = function (){
        $scope.$broadcast('timer-start');
        $scope.timerRunning = true;
    };

    $scope.stopTimer = function (){
        $scope.$broadcast('timer-stop');
        $scope.timerRunning = false;
    };

    $scope.$on('timer-stopped', function (event, data){
        console.log('Timer Stopped - data = ', data);
        $scope.time = data.millis;
        gameOver();
    });


   $scope.getOptions = function(qId, ans){
        console.log("getOptions");
       /*
        if(qId == undefined ){
            return [];
        }
        */
        // If we got a new question we shuffle, otherwise just return previous options
        if($scope.optionsShuffled == false) {
            $scope.optionsShuffled = true;
            var arr = [];

            arr.push({answer:ans,incorrect:0});            
            for(var i = 0; i < $scope.answers.length; i++){
                if($scope.answers[i].question == qId && $scope.answers[i].answer != ans){
                    arr.push({answer:$scope.answers[i].answer, incorrect: 0});
                }
            }
            
            $scope.currentOptions = $scope.shuffle(arr);
            return $scope.currentOptions;
        }
        else {
      
            return $scope.currentOptions;
        }
    };

    $scope.getQText = function(qId, uId){
        
        if(qId == undefined || uId == undefined){
            return "";
        }        
              
        var qObj = _.findWhere($scope.general, {_id: qId});
        var uObj = _.findWhere($scope.users, {_id: uId});
        $scope.currentUser = uObj._id;
        return qObj.text.replace('[Person]', uObj.name); 
        
    };

    $scope.selectOption = function(ans, option){
        // anything you want can go here and will safely be run on the next digest.
        $timeout(function() {
            if($scope.timerRunning == false) {
                $scope.startTimer();
            }
        });

        console.log(ans, option);
        if(ans == option){
            $scope.points++;
            //alert("correct, points: " + $scope.points + " lives: " + $scope.lives);

            $scope.changeToRandomIndex();
            $scope.optionsShuffled = false;
        }
        else{   

            $scope.lives--;

            /*
             * If the answer is incorrect, change incorrect to 1,
             * changing the class of answer to btn-danger. This is
             * done with ng-class in quiz.html
             */ 
            _.find($scope.currentOptions, function(item, index) {
                if(item.answer == option) {
                     $scope.currentOptions[index].incorrect = 1;
                }
            });           

            //  alert("incorrect, points: " + $scope.points + " lives: " + $scope.lives);
            if($scope.lives === 0) {

                gameOver();
               
            }
        }
    };

    gameOver = function() {

        $scope.stopTimer();

        // Save the scores, lives (not actually used for anything) & time to scoreService
        scoreService.setScores($scope.points, $scope.lives, $scope.time);
        // after game over, change location
        $location.url('/endgame');
    }


    $scope.shuffle = function(array) {
        var currentIndex = array.length, temporaryValue, randomIndex ;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    };



    $scope.increment = function() {
        $scope.currentIndex = $scope.currentIndex + 1;
    };

        $scope.decrement = function() {
        $scope.currentIndex = $scope.currentIndex - 1;
    };

        $scope.getIndex = function() {
        return $scope.currentIndex;
    };
}]);
