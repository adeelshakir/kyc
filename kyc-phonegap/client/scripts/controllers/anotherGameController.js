'use strict';

angular.module('kyc-phonegap')
.controller('anotherGameController',['$scope','$http','$location','scoreService','gameTypeService','$timeout','dbService','remoteService','$q',
function($scope, $http,$location, scoreService, gameTypeService, $timeout,dbService,remoteService,$q) {
    
  console.log('anotherGameController Initialized');

  $scope.questions = [];
  var wrongAnswers = [];

  $scope.answers = [];
  $scope.general = [];
  $scope.users = [];
  
  $scope.currentIndex = 0;
  var storedIndex = 0;

  $scope.lives = 3;
  $scope.correctAnswers = 0;
  $scope.incorrectAnswers = 0;
  $scope.time = 0;
  
  $scope.gameType = 0;
  $scope.questionTypes = [true,false,false];

  $scope.timeLost = false;


  var timerRunning = false;

  /*
  * Prepare the questions by merging questions & answers.
  * Answers are shuffled before pushing them
  * Questions are shuffled after the array is ready
  */
  $scope.facegame = function () {
    var users = [];
    var currentId = 0;
    var pic, name, id;
    var ownAnswersIndex = 0;

    var image;
    for(var x = 0; x < $scope.users.length; x++) {
      users.push({id: $scope.users[x]._id,
                  name: $scope.users[x].fname + " " + $scope.users[x].lname,
                  sex: $scope.users[x].sex, photo: $scope.users[x].photo
                  });
    };

    if($scope.questionTypes[0] == true) {
      for(var i = 0; i < users.length; i++) {
        // get sex of current user
        var sex = users[i].sex;
        //sex = sex <= 70 ? 0 : 1;
        
        /* return all the users with selected sex, excluding the current one */
        var names = _.filter(users, function(user) {
            if(user.sex == sex && user.id != users[i].id) {
              return user;
            }
            
        });

        /* Pick 3 randoms from filtered users */
        names = _.sample(names,3);

        // Add current user to names
        names.push(users[i]);

        var incorrect = [0,0,0,0];

        /* Return all "who am i" -question & pick one at random*/    
        var quest = _.filter($scope.general, function(z) {
          return z.type == 1;
        })
        var quest = _.sample(quest, 1);

        /* push question text, question type, answers and picture to questions array */
        $scope.questions.push({question:quest[0].text,
                              type:1,
                              answers:_.shuffle(names),
                              user: users[i].photo,
                              id: users[i].id,
                              incorrect: incorrect        
        });        
      }
    }
    
    if($scope.questionTypes[2] == true) {

      // Find all user made questions.
      var quest = _.filter($scope.general, function(z) {
          return z.type == 2;
      });

      for(var i = 0; i < quest.length; i++) {
        console.log("Q: " + quest[0].text);

        var user = _.findWhere(users, {id:quest[i].user});

        var answers = _.filter($scope.answers, function(a) {
          return a.questionId == quest[i]._id;
        });

        var incorrect = [0,0,0,0];
        $scope.questions.push({question:quest[i].text,
                              type:2,
                              answers:_.shuffle(answers[0].answers),
                              user:user.photo,
                              incorrect: incorrect
                              });

        ownAnswersIndex += 1;
      }   
    }
    
    $scope.questions = _.shuffle($scope.questions);

  };

  $scope.getFace = function() {
    dbService.questions().then(function(data) {
      $scope.general = data.questions;
      console.log("general: " + $scope.general.length);
          return dbService.users();
        }).then(function(data) {
          $scope.users = data.users;
          return dbService.answers();
        }).then(function(data) {
          $scope.answers = data.answers;
        }).finally(function() {
          $scope.facegame();
        });
  }
  $scope.getFace();

  $scope.getAll = function(){
    dbService.questions().then(function(data) {
      $scope.general = data.questions;
        return dbService.answers();
      }).then(function(data) {
        $scope.answers = data.answers;
          return dbService.users();
        }).then(function(data) {
          $scope.users = data.users;
        }).finally(function() {
          $scope.initialize();
        });
  };

  // Set options for different game types. 
  $scope.gameSetup = function() {
    $scope.gameType = gameTypeService.getType();
    $scope.questionTypes = gameTypeService.getQuestionTypes();
   
    console.log("qTypes: " + JSON.stringify($scope.getQuestionTypes));

    if($scope.gameType == 0) {
        $scope.lives = 3;
    }

    if($scope.gameType == 1) {
        $scope.lives = -1;
        $scope.$broadcast('timer-set-countdown-seconds', 60);
        
    }

    if($scope.gameType == 2) {
        $scope.lives = 1;
    }
  };
  $scope.gameSetup();
     
  /* functions for controlling the timer */
   
  $scope.startTimer = function (){
      $scope.$broadcast('timer-start');
      timerRunning = true;
  };

  $scope.stopTimer = function (){
      $scope.$broadcast('timer-stop');
      timerRunning = false;
  };

  $scope.$on('timer-stopped', function (event, data){
      console.log('Timer Stopped - data = ', data);
      $scope.time = data.millis;
  });
    

  var wrongQuestionsShown = false;
  var showWrongs = false;
  /* Check the answer.
   * correct is a boolean used with user-made questions 
   * userId & answerId are used with face & general questions.
   */
  
  $scope.selectOption = function(correct,userId, answerId, index) {
    console.log(correct + " " + userId + " " + answerId + " " + index);
    // Start the timer after first answer
    if(timerRunning == false) {
      $scope.startTimer();
    }
    
    /* If the answer is correct, move to the next question and award points,
     * wrong answer reduces lives and sets incorrect to 1 to apply different button class
     */
    if(correct || (userId == answerId)) {
      $scope.currentIndex++;

      if(showWrongs == false && wrongQuestionsShown == false) {
        $scope.correctAnswers++;  
      }
      
      if($scope.currentIndex == $scope.questions.length && wrongAnswers.length > 0) {
        showWrongs = true;
      }

      if(!showWrongs && wrongQuestionsShown) {
        $scope.gameOver();
      }

      if(showWrongs) {
        console.log("showing wrongs");
        // store the index we're currently at
        storedIndex = $scope.currentIndex;

        // Randomize the order of wrong answers
        wrongAnswers = _.shuffle(wrongAnswers);

        // Pick & remove the first index in the array
        $scope.currentIndex = wrongAnswers.shift();

        // reset incorrect flags
        $scope.questions[$scope.currentIndex].incorrect = [0,0,0,0];

        console.log("we have incorrect answers!");  
        if(wrongAnswers.length == 0) {
          showWrongs = false;
          wrongQuestionsShown = true;
        }

      }

    } else {

        $scope.timeLost = !$scope.timeLost;

        // Save the currentIndex on wrong answers, only save it once per question
        if(wrongAnswers[wrongAnswers.length-1] != $scope.currentIndex) {
          wrongAnswers.push($scope.currentIndex);
        }
        

        $scope.questions[$scope.currentIndex].incorrect[index] = 1;
        $scope.incorrectAnswers++;
        $scope.lives--;     

        if($scope.gameType == 1) {
          $timeout(function() {
            $scope.$broadcast('timer-add-cd-seconds', -1);
            $scope.timeLost = !$scope.timeLost;
          },200); 
        }       
      }
    
      /* After game over, stop the timer, save scores & time and go to the high score page */
    if($scope.lives == 0 || $scope.currentIndex == $scope.questions.length) {
      $scope.gameOver();
    }
  };

  /* This method is called after Time Attack's timer is done */
  $scope.done = function() {
    $timeout(function() {
      $scope.gameOver();
    });
  };

  $scope.gameOver = function() {
    if(timerRunning == true) {
      $scope.stopTimer();
    }
     
    // Save the scores & time to scoreService
    scoreService.setScores($scope.correctAnswers, $scope.incorrectAnswers, $scope.time);
    // after game over, change location
    $location.url('/endgame');
  };

}]);