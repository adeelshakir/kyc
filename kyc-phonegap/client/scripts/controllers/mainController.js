'use strict';

angular.module('kyc-phonegap')
.controller('mainController', ['$scope','$modal','gameTypeService', function($scope, $modal, gameTypeService) {
	
	$scope.game = function(game) {
		gameTypeService.setType(game);
		gameTypeService.setQuestionTypes([true,false,false]);
		gameTypeService.setCustom(false);
	};

	// Modal controller
	$scope.animationsEnabled = true;
	$scope.template = "quickGameModalContent.html";

	$scope.open = function (id, size) {

		// Choose which modal 
	 	switch(id) {
	  	case 0:
	 		$scope.template = "quickGameModalContent.html";
	 		break;
	 	case 1:
			$scope.template = "timeAttackModalContent.html";
			break;
	 	case 2:
			$scope.template = "suddenDeathModalContent.html";
			break;
		case 3:
			$scope.template = "generalInformationModalContent.html";
			break;
	  } 

	var modalInstance = $modal.open({
	  animation: $scope.animationsEnabled,
	  templateUrl: $scope.template,
	  controller: 'ModalInstanceCtrl',
	  size: size
	});
	};

	$scope.toggleAnimation = function () {
	$scope.animationsEnabled = !$scope.animationsEnabled;
	};
}]);

	// Please note that $modalInstance represents a modal window (instance) dependency.
	// It is not the same as the $modal service used above.
angular.module('kyc-phonegap').controller('ModalInstanceCtrl', function ($scope, $modalInstance) {

	$scope.ok = function () {
		$modalInstance.close();
	};
});