

var app = angular.module('kyc-phonegap');

app.controller('parseController',['$scope','$http','remoteService', function($scope,$http,remoteService) {
	
	var user = {};
	var users = [];

	var zip;
	var combining = /[\u0300-\u036F]/g; 
	
	// Array containing the state of each checkbox, default = false
	var checkedBoxes = [];
	$scope.checkedAmount = 0;

	$scope.allImages = [];
	$scope.csvImages = [];

	// Variables for holding the index of dropdowns
	var fname, lname, firstlastname, lastFirstName, email, position, sex;
	// Values for dropdown menus
	$scope.values = ['First name','Last name','First & Last name','Last & First name','Email','Position','Sex',null];
	
	$scope.sex = {s:null};


	// Default value for checkbox in table header
	$scope.allEntries = true;
	
	// Array for parsed data
	$scope.parsed = [];

	/* pagination variables */
	$scope.paginated = []; // spliced array with size of itemsPerPage
	$scope.currentPage = 1; 
	$scope.itemsPerPage = 10; // How many items are shown on one page
	$scope.totalItems = 0; // Total amount of items in CSV. This gets updated in $scope.parse

	/* Parse the given file. if URL is entered, use it to download the CSV,
	 * otherwise use the local file */
	$scope.parse = function() {
		Papa.parse(csv, {
			// header: doesn't work, because the resulting array is sorted differently
			worker: true,
			complete: function(results) {
				$scope.$apply(function() {
					$scope.parsed = results.data;
					// if the "has header" -checkbox is true, remove first line
					if($scope.hasHeader) {
						$scope.parsed.splice(0,1);
					}
					$scope.totalItems = $scope.parsed.length;
					checkedBoxes = new Array($scope.totalItems+$scope.itemsPerPage);
					for (var i = 0; i < checkedBoxes.length; i++) checkedBoxes[i] = false;
				});				
			}
		});
		
	}

	/* read the files from given file into array */
	$scope.setFiles = function(element) {
		var photofile = element.files[0];
        var reader = new FileReader();
       
        reader.onload = function(e) {
            //console.log("Result: " + e.target.result);
            zip = new JSZip(e.target.result);
            
            /* Look through the filelist for csv and images */
        	_.each(zip.files, function(file,filename) {
        		
        		// Save the csv as text for parsing
        		if(filename.indexOf('csv') > -1) {
        			csv = zip.file(filename).asText();
        		}

        		// Find images by file extension
            	if(filename.indexOf('jpg') > -1) {
            		
            		var name = filename;

            		// Look for slashes and take a substring after last one
 					var slash = name.lastIndexOf('/');
            		if(slash > -1) {
            			name = name.substring(slash + 1);
            		}

            		// Remove file extension
            		name = name.substring(0,name.indexOf("."));

            		name = name.toLowerCase();

            		// Change umlauts (äö...) to closest matching character
 					name = name.normalize('NFKD').replace(combining, '');         		
         		 	
 					var image = new Image();
            		// Encode the image to base64
            		//image.src = "data:image/jpg;base64," + btoa(String.fromCharCode.apply(null, zip.files[filename].asUint8Array()));
            		image.src = "data:image/jpg;base64," + btoa(zip.files[filename].asBinary());

            		$scope.allImages.push({name: name, photo: image});
            		//image = new Image();
            	} 	
             }); 
         };

         reader.onerror = function(e) {
         	console.log("Error: " + e.target.error);
         }

        reader.readAsArrayBuffer(photofile);	      
    };

    /* Find the photo of the given user */
    $scope.getPhoto = function(name) {

    	var userPhoto;

    	// Switch the order of first & last name 
    	var splitName = name.split(" ");
    	var flippedName = splitName[1] + " " + splitName[0];
	
		name = name.toLowerCase();
		flippedName = flippedName.toLowerCase();

		// Change umlauts (äö...) to closest matching character
		name = name.normalize('NFKD').replace(combining, '');
		flippedName = flippedName.normalize('NFKD').replace(combining, '');

		/* Look for matching name and, if found, return user's base64-encoded
		 * photo. The photo is looked for both ways, first name & last name and
		 * last name & first name
		 */
    	var user = _.findWhere($scope.allImages, {name:name});
    	if(user) {
    		// return the base64-encoded image
    		userPhoto = user.photo.src;
    	} else {
    		user = _.findWhere($scope.allImages, {name:flippedName});
    		if(user) {
    			userPhoto = user.photo.src; 
    		}
    		  		
    	}

    	return userPhoto;

    }

    /* save the index of the column that contains a wanted value */
    $scope.getVal = function(index, value) {

    	index--;
		if(value == 'First name') {
			fname = index;
    	}
    	if(value == 'Last name') {
    		lname = index;
    	}
    	if(value == 'Email') {
    		email = index;
    	}
    	if(value == 'Position') {
    		position = index; 
    	};
    	if(value == 'First & Last name') {
    		firstlastname = index;
    	}
    	if(value == 'Last & First name') {
    		lastFirstName = index;
    	}

    };


    /* Keep track on enabled checkboxes. Parameters are the index of the
     * checkbox, its state (true/false) and current page of paginated
     */ 
    $scope.checkedBoxes = function(index, checked, currentPage) {
    	// Set allEntries to false. This is the state of the checkbox in table header
    	$scope.allEntries = false;

    	/* Subract the value of currentPage by 1 and then multiply it by 10, so each checkbox gets unique value. 
		 * from 0 to n. 
    	*/
    	currentPage -= 1;
    	currentPage *= 10;

    	// Flip the truth value. 
    	checkedBoxes[currentPage + index] = !checkedBoxes[currentPage + index];

    	// Add, if user was selected. Substract, if user was deselected
    	checkedBoxes[currentPage + index] ? $scope.checkedAmount++ : $scope.checkedAmount--;
    };

    // Return the state of checkbox
    $scope.isChecked = function(index, currentPage) {
    	
    	currentPage -= 1;
    	currentPage *= 10;

    	// return 
    	return checkedBoxes[currentPage + index];
    }

    /* Fill the users array with user values */
    $scope.add = function() {

    	var name;
    	var image;

    	if(!$scope.sex.s || sex) {
    		alert("No sex defined, use either radio button or dropdown to choose");
    		return "";
    	}
    
    	if($scope.allEntries) {
	    	_.each($scope.parsed, function(u,i) {
	    		// Check that the entry hasn't been previously added
	    	
	    		if(u.added != true) {
		    		if(firstlastname >= 0) {
		    			var names = u[firstlastname].split(" ");
		    			user.fname = names[0];
		    			user.lname = names[1];
		    		} else if (lastFirstName >=0) {
		    			var names = u[lastFirstName].split(" ");
		    			user.fname = names[1];
		    			user.lname = names[0];
		    		} else {
		    			user.fname = u[fname];
		    			user.lname = u[lname];
		    		}
		    		name = user.fname + " " + user.lname;
		    		name = name.toLowerCase();
					// Change umlauts (äö...) to closest matching character
					name = name.normalize('NFKD').replace(combining, '');
		    		
		    		user.email = u[email];
		    		user.position = u[position];
		    		
		    		
		    		// Sex is defined by the radio button
		    		if($scope.sex.s != null) {
		    			if($scope.sex.s === "male") {
		    				user.sex = 0;
		    			} else if ($scope.sex.s === "female") {
		    				user.sex = 1;
		    			}
		    		} else {
		    			user.sex = $scope.parsed[i][sex];
		    		}

		    		image = _.findWhere($scope.allImages, {name:name});

		    		if(image) {
		    			user.photo = image.photo.src;
		    			users.push(user);
		    			$scope.parsed[i].added = true;
		    			console.log("pushed: " + name + " sex: " + user.sex);
		    		}
		    		
	    			user = {};
	    			
    			}
    		}); 
    	} else {
    		_.each(checkedBoxes, function(x,i) {
    			if(x == true) {
    				console.log("X: " + x + " i: " + i);
	    			if(firstlastname >= 0) {
		    			var names = $scope.parsed[i][firstlastname].split(" ");
		    			user.fname = names[0];
		    			user.lname = names[1];
		    		} else if(lastFirstName >= 0) {
		    			var names = $scope.parsed[i][lastFirstName].split(" ");
		    			user.fname = names[1];
		    			user.lname = names[0];
		    		} else {
		    			user.fname = $scope.parsed[i][fname];
	    				user.lname = $scope.parsed[i][lname];
		    		}
	    			name = user.fname + " " + user.lname;
	    			name = name.toLowerCase();
					// Change umlauts (äö...) to closest matching character
					name = name.normalize('NFKD').replace(combining, '');

	    			user.email = $scope.parsed[i][email];
	    			user.position = $scope.parsed[i][position];

	    			// Sex is defined by the radio button
		    		if($scope.sex) {
		    			//console.log("sex selected in radio");
		    			if($scope.sex.s === "male") {
		    				user.sex = 0;
		    			} else if ($scope.sex.s === "female") {
		    				user.sex = 1;
		    			}
		    		} else {
		    			user.sex = $scope.parsed[i][sex];
		    		}

	    			image = _.findWhere($scope.allImages, {name:name});

	    			if(image) {
		    			user.photo = image.photo.src;
		    			users.push(user);
		    			console.log("pushed: " + name + " sex: " + user.sex);
	    			}
	    			
	    			user = {};
	    			
	    			// Mark the user as added, so he can't be added again
	    			$scope.parsed[i].added = true;

	    			// Uncheck the checkbox
	    			checkedBoxes[i] = false;

	    			// Reduce the amount of checked by one, this is used to control the 
	    			// allEntries checkbox state.
	    			$scope.checkedAmount--;

    			}
    		});
    	}
    	 
    }

    /* push added users to database */
    $scope.submitToDb = function() {

    	  for(var i = 0; i < users.length; i++) {
    	  	$http.post(remoteService.address() + '/users/save', users[i]).success(function(response) {
				console.log("User saved!");
			});	
    	  }
		
    	
    	/*
		_.each(users, function(user) {
			$http.post(remoteService.address() + '/users/save', user).success(function(response) {
				console.log("User saved!");
			});
		})
		*/
		// Empty the array
		users = [];
    };


    /* This watcher listens for page changes in pagination, $scope.parsed and checkedBoxes
     * $scope.parsed is watched, because it has no size before it's populated
     * by parse. CheckedBoxes is watched to set $scope.allEntries to true after submitting chosen lines
     */
    $scope.$watchGroup(["currentPage + itemsPerPage",'parsed.length','checkedBoxes.length','checkedAmount'], function() {
	    var begin = (($scope.currentPage -1) * $scope.itemsPerPage)
	    , end = begin + $scope.itemsPerPage;
	    $scope.paginated = $scope.parsed.slice(begin, end);

	    if($scope.checkedAmount == 0) {
	    	$scope.allEntries = true;
	    }
  	});
}]);

/* Return an array, that's longer by 1 than the original array */
app.filter('range', function() {
	return function(param) {
		var dropdowns = [];
		var parsed = param.length+2;
		for(var i = 0; i < parsed; i++) {
			dropdowns.push(i);
		}
		return dropdowns;
	};
});