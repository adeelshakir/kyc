'use strict';

var app = angular.module('kyc-phonegap');

app.controller('scoreController',['$scope','$http','scoreService','gameTypeService','highScore','$timeout','remoteService','$location', 
	function ($scope, $http, scoreService, gameTypeService, highScore, $timeout, remoteService, $location) {
	console.log("Score Controller Initialized!");
	

	var remote =  remoteService.address();

	$scope.gameScores = scoreService.getScores();

	$scope.tab = 0;

	$scope.gameModes = ['Quick Play','Time Attack','Sudden Death'];
	$scope.mode = $scope.gameModes[0];
	$scope.gameMode = 0;
	$scope.isCustom = false;


	$scope.user = {
		name: null,
		points: null,
		time: null,
		date: new Date(),
		type:null,
	}

	$scope.hs = 0;
	$scope.minScore = 0;
	$scope.minTime = 0;

	$scope.gameType = function(gametype) {
		highScore.getScores(gametype).then(function(id) {
			$scope.scores = id.data;

			// Update the game mode
			$scope.gameMode = gametype;
			$scope.mode = $scope.gameModes[gametype];
			$scope.isCustom = gameTypeService.getCustom();
		});
	};
	$scope.gameType(gameTypeService.getType());

	$scope.tabs = [
	    { title:'Weekly', content:'' },
	    { title:'Monthly', content:'' },
	    { title:'All-time', content:'' }
	];

	$scope.tabChange = function(tab) {
		$scope.tab = tab;
	}

	$scope.postScores = function() {
		if($scope.user.points == null) {
			$scope.user.points = $scope.gameScores.points;
		}
		if($scope.user.time == null) {
			$scope.user.time = $scope.gameScores.time;
		}
		if($scope.user.type == null) {
			$scope.user.type = gameTypeService.getType();
		}
		$http.post(remote + '/scores', $scope.user).success(function(response) {
			$scope.user = "";
			$location.url('/scores')
		});
	};

	$scope.isHighScore = function() {
    	highScore.getScores(0).then(function(d) {
			console.log("reply: "+d.data[0].points);
			points = d.data;
			$scope.minScore = points[points.length-1].points;
			$scope.minTime = points[points.length-1].time;
			console.log("points: " + points[points.length-1].points);
			if($scope.gameScores.points >  points[points.length-1].points) {
				$scope.hs = 1;
			}
		});
	};
	//$scope.isHighScore();
/*
	$scope.setGameMode = function() {
		$scope.gameMode = gameTypeService.getType();
		$scope.gameType($scope.gameMode);
		$scope.mode = $scope.gameModes[$scope.gameMode];
	};
	$scope.setGameMode();
*/
}]);

app.filter('timespan', function($filter) {
	return function(scores, tab) {	
		var filtered = [];
		var currentDate = new Date();
		var scoreDate = 0;
		// Weekly scores
		if(tab === 0) {
			  // push all scores with week number equal to current dates
			  // number to filtered array
			_.each(scores, function(score) {
				// Convert date string to Date object
				scoreDate = new Date(score.date);
				// Use momentjs isoWeek to get the week numbers. Angulars date-filter doesn't
				// work, because it's first day of the week is Sunday
				if(moment(currentDate).isoWeek() === moment(scoreDate).isoWeek() && moment(currentDate).year() === moment(scoreDate).year()) {
				// use angulars date-filter to get the week number
				//if($filter('date')(scoreDate,'w') === $filter('date')(currentDate,'w')) {
					filtered.push(score);
				}		
			});
		  // Monthly scores
		} else if (tab === 1) {
			// Same as above, but for equal month
			_.each(scores, function(score) {
				// Convert date string to Date object
				scoreDate = new Date(score.date);
				// use angulars date-filter to get the month
				if($filter('date')(scoreDate,'M') === $filter('date')(currentDate,'M') && $filter('date')(scoreDate,'y') === $filter('date')(currentDate,'y')){
					filtered.push(score);
				}
			});
		  // All-time, no need for filtering, return original array
		} else {
			return scores;
		}
		return filtered;
	};
	
});
