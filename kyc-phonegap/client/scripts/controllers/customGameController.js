'use strict'

var app = angular.module('kyc-phonegap');

app.constant('datepickerConfig', {
	formatDay: 'dd',
	formatMonth: 'MMMM',
	formatYear: 'yyyy',
	formatDayHeader: 'EEE',
	formatDayTitle: 'MMMM yyyy',
	formatMonthTitle: 'yyyy',
	datepickerMode: 'month',
	minMode: 'day',
	maxMode: 'year',
	showWeeks: false,
	startingDay: 1,
	yearRange: 20,
	minDate: new Date('01-01-1900'),
	maxDate: null,
	shortcutPropagation: false
});



app.controller('customGameController', ['$scope','gameTypeService', function($scope,gameTypeService) {
	
	$scope.initDate =new Date('01-01-1900');
	$scope.radioModel = 0;

	$scope.start = function() {
		console.log("Mode: " + $scope.radioModel + " Questions: " + JSON.stringify($scope.checkModel));
		gameTypeService.setType($scope.radioModel);
		gameTypeService.setQuestionTypes($scope.checkModel);
		gameTypeService.setCustom(true);
	}


  	$scope.checkModel = [false, false, false];
  	/*
	    face: false,
	    general: false,
	    usermade: false
 	 };
*/
 	$scope.today = function() {
 		$scope.dt = new Date();
  	};
  	$scope.today();

 	$scope.clear = function () {
    	$scope.dt = null;
  	};

  
  	$scope.toggleMin = function() {
    	$scope.minDate = new Date();
  	};

	
  	$scope.open = function($event) {
    	$event.preventDefault();
    	$event.stopPropagation();

    	$scope.opened = true;
  	};

  	$scope.dateOptions = {
    	formatYear: 'yy',
   		startingDay: 1,
  	};

  	// ḿarking
  	var tomorrow = new Date();
  	tomorrow.setDate(tomorrow.getDate() + 1);
  	var afterTomorrow = new Date();
  	afterTomorrow.setDate(tomorrow.getDate() + 2);
  	$scope.events =
    [
      {
        date: tomorrow,
        status: 'full'
      },
      {
        date: afterTomorrow,
        status: 'partially'
      }
    ];

  	$scope.getDayClass = function(date, mode) {
    	if (mode === 'day') {
	      		var dayToCheck = new Date(date).setHours(0,0,0,0);

	      	for (var i=0;i<$scope.events.length;i++){
	        	var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

	        	if (dayToCheck === currentDay) {
	        		return $scope.events[i].status;
	        	}
	      	}
    	}

   		return '';
  	};

}]);