'use strict';

angular.module('kyc-phonegap')
.controller('addQuestionController',['$scope','dbService','$http','$route','remoteService',  function($scope, dbService,$http,$route,remoteService) {
	console.log("addQuestionController initialized!");
	
	var remote =  remoteService.address();

	$scope.users;
	$scope.q = {
		userId: null,
		question: null,
		answers: []
	};

	// get all users
	dbService.users().then(function(d) {
        $scope.users = d.users;
    });

    $scope.postQuestion = function() {


       	console.log("userId: " + $scope.q.userId);
    	console.log("question: " + $scope.q.question);



    	$scope.q.answers.push({option: $scope.q.answer, correct: true});
    	$scope.q.answers.push({option: $scope.q.secondary, correct: false});

    	if($scope.q.optional1) {
    		$scope.q.answers.push({option: $scope.q.optional1, correct: false});
    	}
    	
    	if($scope.q.optional2) {
    		$scope.q.answers.push({option: $scope.q.optional2, correct: false});
    	}
    	

       	console.log("answers: " + JSON.stringify($scope.q.answers));

        var postUser = angular.copy($scope.q);
       
    	$http.post(remote + '/questions/add', JSON.stringify(postUser)).success(function(response) {        
            
		});
        $scope.addQuestionForm.$setPristine();
		$scope.q = {
            userId: postUser.userId,
            question: null,
            answers: []
        };
    };



}]);
