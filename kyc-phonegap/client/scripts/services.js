'use strict';

var app = angular.module('services',[])

var remote = 'http://kyc-kycdemo.rhcloud.com';
//var remote = 'http://localhost:8080';
//var remote = 'http://192.168.1.78:8080';
/** 
 * scoreService stores the scores to transfer them
 * between different controllers. 
 */
app.service('scoreService', function() {
  var prop = {};

  this.setScores = function(points, lives, time) {
    prop.points = points;
    prop.lives = lives;
    prop.time = time;
  };

  this.getScores = function () {
    return prop;
  };

})

app.service('remoteService', function() {
  return {
    address: function() {
      return remote;
    }
  };

});

app.service('gameTypeService', function() {

  var type = 0;
  var questions = [];
  var customGame = false;

  this.setType = function(t) {
    this.type = t;
  };

  this.getType = function() {
    return this.type;
  };

  this.setQuestionTypes = function(q) {
    this.questions = q;
  };

  this.getQuestionTypes = function() {
    return this.questions;
  }

  this.setCustom = function(isCustom) {
    this.customGame = isCustom;
  }

  this.getCustom = function() {
    return this.customGame;
  }

});

app.service('highScore', function($http) {

  var highScoreService = {
    getScores: function(id) {
      // $http returns a promise, which has a then function, which also returns a promise
      var promise = $http.get(remote + '/scores' + id).then(function (response) {
        // The then function here is an opportunity to modify the response
        //console.log(JSON.stringify(response));
        // The return value gets picked up by the then in the controller.
        return response;
      });
      // Return the promise to the controller
      return promise;
    }
  };
  return highScoreService;
});

/*
app.service('users', function($http) {
  var usersPromise;
  var getUsers {

    if(!usersPromise) {
      usersPromise = $http.get(remote + '/users/all').then(function (response) {
        // The then function here is an opportunity to modify the response
        //console.log(JSON.stringify(response.users));
        if(response.error) { console.log("Err: " + response.error);}
          // The return value gets picked up by the then in the controller.
          console.log("Returning users");
          return response.data;
        });
    }
  
   return usersPromise;
  }
}
*/

app.service('dbService2', function($http) {
  var usersPromise, questionsPromise,questionsByTypePromise, answersPromise;

  this.getUsers = function() {
    if( !usersPromise) {
      // $http returns a promise, which has a then function, which also returns a promise
      usersPromise = $http.get(remote + '/users/all').then(function (response) {
        // The then function here is an opportunity to modify the response
        //console.log(JSON.stringify(response.users));
        if(response.error) { console.log("Err: " + response.error);}
        // The return value gets picked up by the then in the controller.
        console.log("Returning users");
        return response.data;
      });
    }
    // Return the promise to the controller
    console.log("returning  users promise");
    return usersPromise;
  }

  this.getQuestions = function() {
    if( !questionsPromise) {
      questionsPromise = $http.get(remote + '/questions/get').then(function (response) {
        // The then function here is an opportunity to modify the response
        // console.log(JSON.stringify(response.users));
        // The return value gets picked up by the then in the controller.
        console.log("Returning questions");
        return response.data;
      });
    }

    // Return the promise to the controller
    console.log("returning  questions promise");
    return questionsPromise;
  }

});


app.service('dbService', function($http) {

  var usersPromise, questionsPromise,questionsByTypePromise, answersPromise;

  var dbService = {

    users: function() {
      if( !usersPromise) {
          // $http returns a promise, which has a then function, which also returns a promise
          usersPromise = $http.get(remote + '/users/all').then(function (response) {
          // The then function here is an opportunity to modify the response
          //console.log(JSON.stringify(response.users));
          if(response.error) { console.log("Err: " + response.error);}
          // The return value gets picked up by the then in the controller.
          console.log("Returning users");
          return response.data;
        });
      }
      // Return the promise to the controller
      console.log("returning  users promise");
      return usersPromise;
    },
    questions: function() {
      if( !questionsPromise) {
        questionsPromise = $http.get(remote + '/questions/get').then(function (response) {
          // The then function here is an opportunity to modify the response
         // console.log(JSON.stringify(response.users));
          // The return value gets picked up by the then in the controller.
          console.log("Returning questions");
          return response.data;
        });
      }
      // Return the promise to the controller
      console.log("returning  questions promise");
      return questionsPromise;
    },
    questionsByType: function(type) {
       if( !questionsByTypePromise) {
        questionsByTypePromise = $http.get(remote + '/questions/get/' + type).then(function (response) {
          console.log("Returning questionsByType");
          return response.data;
        });
      }
      console.log("Returning questionsByType Promise");
      return questionsByTypePromise;
    },
    answers: function() {
      if( !answersPromise) {
        answersPromise = $http.get(remote + '/ownAnswers/get').then(function (response) {
        // The then function here is an opportunity to modify the response
        // console.log(JSON.stringify(response.users));
        // The return value gets picked up by the then in the controller.
          console.log("Returning answers");
          return response.data;
        });
      }
      // Return the promise to the controller
      console.log("returning  answers promise");
      return answersPromise;
    }
  };
  console.log("returning service");
  return dbService;
});
