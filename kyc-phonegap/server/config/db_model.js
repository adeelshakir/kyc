var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var scoreSchema = new Schema({
	name: String,
	points: Number,
	time: Number,
	date: Date,
	type: Number
}, {collection: 'scores'});

var ownQuestionSchema = new Schema({
	text: String,
	user: String,
	type: Number
}, {collection: 'questions'});

var answerArraySchema = new Schema({option: String, correct: Boolean });

var ownAnswerSchema = new Schema({
	questionId: Schema.Types.ObjectId,
	answers: [answerArraySchema]
}, {collection: 'ownAnswers'});


var userSchema = new Schema({
	fname: String,
	lname: String,
	sex: Number,
	photo: String
}, {collection: 'users'});

var scores = mongoose.model("scores", scoreSchema);
var ownQuestion = mongoose.model("ownQuestion",ownQuestionSchema);
var ownAnswers = mongoose.model("ownAnswers",ownAnswerSchema);

var questionSchema = new Schema({}, { strict: false });
var question = mongoose.model("question", questionSchema);

var generalSchema = new Schema({}, { strict: false });
var general = mongoose.model("general", generalSchema);
//var userSchema = new Schema({}, { strict: false });
var user = mongoose.model("user", userSchema);
var answerSchema = new Schema({}, { strict: false });
var answer = mongoose.model("answer", answerSchema);