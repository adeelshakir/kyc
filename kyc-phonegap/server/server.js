#!/bin/env node
require("./config/db_model");

var express = require('express');
var fs      = require('fs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bodyParser = require('body-parser');

var ipaddr  = process.env.OPENSHIFT_NODEJS_IP;
var port    = parseInt(process.env.OPENSHIFT_NODEJS_PORT) || 8080;
  
var conn = process.env.OPENSHIFT_MONGODB_DB_URL + "kyc";


mongoose.connect(conn); 
//mongoose.connect('mongodb://localhost/kyc');


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Mongoose connected!");
});

var app = express();


  //var methodOverride = require('method-override');
  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: true }));
  // parse application/json
  app.use(bodyParser.json({limit: '15mb'}));


  app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next(); // ???
  });

var scoreModel = mongoose.model('scores');
var genModel = mongoose.model('general');
var userModel = mongoose.model('user');
var ansModel = mongoose.model('answer');
var questionModel = mongoose.model('ownQuestion');
var ownAnswersModel = mongoose.model('ownAnswers');

app.get('/', function(req, res) {
  res.send("101");
});

/* Return all scores  */
app.get('/scores', function(request, response) {
  console.log("GET request received!");
  scoreModel.find({}, function(err, scores) {
    response.send(scores);
  }).sort({points: -1, lives: -1, time: 1});
});

 // Find the scores by type. 0 = quiz, 1 = time attack, 2 streak
// Send found scores back sorted by points, lives and time.
app.get('/scores:type', function(request,response) {
  var type = request.params.type; // Param from scoreController, value 0, 1 or 2

  scoreModel.find({type: type}, function(err, scores) {
    response.send(scores);
  });
});

app.post('/scores', function(request, response) {
  scoreModel.create(request.body, function(err, score) {
    response.send('success');
  });
});

/* Return all the user-made questions */
app.get('/questions/get', function(request, response) {
  questionModel.find({}, function(err, docs) {
    if(err) {
      response.json({error: true, questions: []});
    } else {
      response.json({error: false, questions: docs});
    }
  });
});

app.get('/questions/get/:type', function(request, response) {
  console.log("Type: " + request.params.type);
  var type = request.params.type;
  questionModel.find({type: type}, function(err, docs) {
    if(err) {
      response.json({error: true, questions: []});
    } else {
      response.json({error: false, questions: docs});
    }
  });
});

/* Return all the user-made answers */
app.get('/ownAnswers/get', function(request, response) {
  ownAnswersModel.find({}, function(err, docs) {
    if(err) {
      response.json({error: true, answers: []});
    } else {
      response.json({error: false, answers: docs});
    }
  });
});


/* Add a user-made question */
app.post('/questions/add', function(request, response) {
 
  /* get question, answers and userId from the request */
  var text = request.body.question;
  var user = request.body.userId;
  var answers = request.body.answers;
  var qObj;

  /* First, insert the question to collection questions. Parameters are question, userId and type of
   * the question which, in this case, is always 2.
   */ 
  questionModel.create({text: text, user: user, type:2}, function(err, doc) {
    if(err) console.log(err);
    else {
      console.log(doc);
      /* Save the ObjectID of the created question */
      qObj = doc._id;
      console.log("QOBJ: " + qObj);
    };

    /* Insert the answer array to collection ownAnswers. Parameters are the ObjectID of
     * the question and the array containing the answers.
     */
    ownAnswersModel.create({questionId: qObj,answers:answers}, function(err, docs) {
        if(err) console.log(err);
        else {
        console.log(docs);
      };
    });
  });
});

app.post('/users/save', function(request, response) {

  var fname = request.body.fname;
  var lname = request.body.lname;
  var photo = request.body.photo;
  var sex   = request.body.sex;

  userModel.create({fname: fname, lname: lname, photo: photo, sex: sex}, function(err, doc) {
    if(err) console.log(err);
    else {
      console.log("OK");
      response.send('success');
    }
  });


    /*
    userModel.findOne({name: req.body.name}, function(err, doc){
       if(doc == null){
           new userModel(req.body).save(function(err, ndoc){
               res.json({error: false, user: ndoc, new: true});
           });
       }else{
           res.json({error: false, user: doc, new: false});
       }
    });
*/
});

app.get('/general/all', function(req, res) {
    genModel.find({}, function(err, docs){
        if(err){
            res.json({error: true, general: []});
        }else{
            res.json({error: false, general: docs});
        }
    });
});
app.get('/answers/all', function(req, res) {
    ansModel.find({}, function(err, docs){
        if(err){
            res.json({error: true, answers: []});
        }else{
            res.json({error: false, answers: docs});
        }
    });
});
app.get('/users/all', function(req, res) {
    userModel.find({}, function(err, docs){
        if(err){
            res.json({error: true, users: []});
        }else{
            res.json({error: false, users: docs});
        }
    });
});

  
  //starting the nodejs server with express
app.listen(port, ipaddr, function(){
      console.log('%s: Node server started on %s:%d ...', Date(Date.now()), ipaddr, port);
});


